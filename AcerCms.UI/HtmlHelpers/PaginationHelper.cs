﻿using System.Web.Mvc;

namespace AcerCms.UI.HtmlHelpers
{
    public static class PaginationHelper 
    {
        public static MvcHtmlString Pagination(this HtmlHelper html)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);

            string pagination = @"<nav>
                    <ul class='pagination'>                        
                        <li><a href='"+url.Action("Index", "Haber", new {page = 1})+"'>1</a></li>"+
                        "<li><a href='"+url.Action("Index", "Haber", new {page = 2})+"'>2</a></li>"+
                        "<li><a href='"+url.Action("Index", "Haber", new {page = 3})+"'>3</a></li>"+
                        "<li><a href='"+url.Action("Index", "Haber", new {page = 4})+"'>4</a></li>"+
                        "<li><a href='"+url.Action("Index", "Haber", new {page = 5})+@"'>5</a></li>
                    </ul>
                </nav>";

            return MvcHtmlString.Create(pagination);
        }

//        public static MvcHtmlString Pager(this HtmlHelper html, 

//                                          PagingInfo pagingInfo, 

//                                          Func<int, string> pageURL,

//                                          string ulCssClass = "pagination",

//                                          int linkCount = 10)

//        {

//            //if page count == 1 we don't need pager

//            if (pagingInfo.TotalPages <= 1) return MvcHtmlString.Create("");





//            StringBuilder result = new StringBuilder();



//            TagBuilder ul = new TagBuilder("ul");

//            ul.AddCssClass(ulCssClass);

            

//            ///Adding previous and next page links

//            TagBuilder aPreNextPagerLinks = new TagBuilder("a");



//            //previous (linkCount) 10 pager links

//            if (pagingInfo.CurrentPage > linkCount)

//                aPreNextPagerLinks.MergeAttribute("href", pageURL(pagingInfo.CurrentPage - linkCount));

//            else if(pagingInfo.CurrentPage == 1)

//                aPreNextPagerLinks.MergeAttribute("href", "javascript:;");

//            else

//                aPreNextPagerLinks.MergeAttribute("href", pageURL(1));



//            aPreNextPagerLinks.SetInnerText("««");

//            result.Append("<li>" + aPreNextPagerLinks.ToString() + "</li>");

                                   

//            //single page previous

//            TagBuilder aPre_Next = new TagBuilder("a");

            

//            //previous

//            if (pagingInfo.CurrentPage > 1)

//                aPre_Next.MergeAttribute("href", pageURL(pagingInfo.CurrentPage - 1));

//            else

//                aPre_Next.MergeAttribute("href", "javascript:;");



//            aPre_Next.SetInnerText("«");

//            result.Append("<li>" + aPre_Next.ToString() + "</li>");            

            

//            ///adding numbered page links            

//            //calculating start and end of pager links

//            int start = ((pagingInfo.CurrentPage-1) / linkCount ) * linkCount + 1;

            

            

//            int end = start + linkCount - 1;



//            if (end > pagingInfo.TotalPages)

//                start -= (end - pagingInfo.TotalPages);

            



//            if (start <= 0) start = 1;

            



//            for (int i = start ; i <= end && i<= pagingInfo.TotalPages; i++)

//            {

//                TagBuilder li = new TagBuilder("li");

//                TagBuilder a = new TagBuilder("a");               

             

//                a.MergeAttribute("href", pageURL(i));

//                a.SetInnerText(i.ToString());



//                li.InnerHtml = a.ToString();



//                if (i == pagingInfo.CurrentPage) li.AddCssClass("active");



//                result.Append(li.ToString());                

//            }





//            //next

//            aPre_Next = new TagBuilder("a");



//            if (pagingInfo.CurrentPage < pagingInfo.TotalPages)

//                aPre_Next.MergeAttribute("href", pageURL(pagingInfo.CurrentPage + 1));

//            else

//                aPre_Next.MergeAttribute("href", "javascript:;");



//            aPre_Next.SetInnerText("»");

//            result.Append("<li>" + aPre_Next.ToString() + "</li>"); 

            

//            //next (linkCount) 10 pager links

//            aPreNextPagerLinks = new TagBuilder("a");



//            //previous (linkCount) 10 pager links

//            if (pagingInfo.CurrentPage < (pagingInfo.TotalPages - linkCount) )

//                aPreNextPagerLinks.MergeAttribute("href", pageURL(pagingInfo.CurrentPage + linkCount));

//            else if (pagingInfo.CurrentPage == pagingInfo.TotalPages)

//                aPreNextPagerLinks.MergeAttribute("href", "javascript:;");

//            else

//                aPreNextPagerLinks.MergeAttribute("href", pageURL(pagingInfo.TotalPages));



//            aPreNextPagerLinks.SetInnerText("»»");

//            result.Append("<li>" + aPreNextPagerLinks.ToString() + "</li>");



//            //---------------------- PageSize Chaneger -----------------------//

//            StringBuilder pageSizeChanger = new StringBuilder();

//            pageSizeChanger.Append(@"<div class=""btn-group dropup"" style=""display: inline-block; margin: 20px 30px;"">
//
//                                     <button type=""button"" 
//
//                                        class=""btn btn-default dropdown-toggle"" data-toggle=""dropdown""> Sayfa Büyüklüğü <span class=""caret""></span></button>
//
//                                     <ul class=""dropdown-menu"" role=""menu"">");



//            string currentRequest = html.ViewContext.RequestContext.HttpContext.Request.Path;           

//            string pagesize10 ="?pagesize=10";

//            string pagesize20 = "?pagesize=20";

//            string pagesize50 = "?pagesize=50";

//            string pagesize100 = "?pagesize=100";           

            

//            pageSizeChanger.Append(string.Format(@"
//
//                                        <li {0}><a href=""{1}"">10</a></li>
//
//                                        <li {1}><a href=""{3}"">20</a></li>
//
//                                        <li {4}><a href=""{5}"">50</a></li>                                        
//
//                                        <li {6}><a href=""{7}"">100</a></li>
//
//                                    </ul></div>",

//                                                (pagingInfo.ItemsPerPage == 10) ? "class=\"active\"" : "",

//                                                pagesize10,

//                                                (pagingInfo.ItemsPerPage == 20) ? "class=\"active\"" : "",

//                                                pagesize20,

//                                                (pagingInfo.ItemsPerPage == 50) ? "class=\"active\"" : "",

//                                                pagesize50,

//                                                (pagingInfo.ItemsPerPage == 100) ? "class=\"active\"" : "",

//                                                pagesize100));

            

//            //-------------------- /Page Size Changer --------------------------//



//            //-------------- "items showing" text ----------------------------//

//            StringBuilder divItemCount = new StringBuilder();

//            int startIndex = pagingInfo.CurrentPage * pagingInfo.ItemsPerPage - pagingInfo.ItemsPerPage + 1;

//            int endIndex = startIndex + pagingInfo.ItemsPerPage - 1;



//            divItemCount.Append(@"<div class=""pagination"" style=""float: right;"">");

//            divItemCount.Append(string.Format(@"<p style=""margin: 10px;"">{0} Kayıttan {1}-{2} Gösteriliyor.</p></div>", pagingInfo.TotalItems, startIndex, endIndex));



//            //-------------- /"items showing" text ----------------------------//



//            ul.InnerHtml = result.ToString();



//            StringBuilder pagination = new StringBuilder();

//            pagination.Append(ul.ToString());

//            pagination.Append(pageSizeChanger.ToString());

//            pagination.Append(divItemCount);



//            return MvcHtmlString.Create(pagination.ToString());

//        }
    }
}