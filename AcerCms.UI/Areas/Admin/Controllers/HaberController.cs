﻿using System;
using System.Data.Entity.Validation;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using AcerCms.Dal;
using AcerCms.Dal.Enities;
using AcerCms.UI.Areas.Admin.Models;
using AcerCms.UI.Areas.Admin.ModelServices;
using Microsoft.AspNet.Identity;


namespace AcerCms.UI.Areas.Admin.Controllers
{
    [Authorize]
    public class HaberController : BaseController
    {
        // GET: Admin/Haber
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index(int page = 1)
        {
            var model = HaberModelService.GetNews(page);           
            return View(model);
        }

        //[NonAction]
        public string AddSomeNews()
        {
            var userId = User.Identity.GetUserId();
            var news = new News[]
            {
                new News() {  Title = "1. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0, PersonId = userId},
                new News() {  Title = "2. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0, PersonId = userId},
                new News() {  Title = "3. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0, PersonId = userId},
                new News() {  Title = "4. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0, PersonId = userId},
                new News() {  Title = "5. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0, PersonId = userId},
                new News() {  Title = "6. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0, PersonId = userId},
                new News() {  Title = "7. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0, PersonId = userId},
                new News() {  Title = "8. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0, PersonId = userId},
                new News() {  Title = "9. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0, PersonId = userId},
                new News() {  Title = "10. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0,PersonId = userId},
                new News() {  Title = "11. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0,PersonId = userId},
                new News() {  Title = "12. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0,PersonId = userId},
                new News() {  Title = "13. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0,PersonId = userId},
                new News() {  Title = "14. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0,PersonId = userId},
                new News() {  Title = "15. haber",  Content = "skj hkjahskjhkjhksjd", ShortDescription = "hjda gfjhgj", UpdateDate = DateTime.Now, ViewCount = 0,PersonId = userId}
            };

            using (var db = new ApplicationDbContext())
            {
                db.News.AddRange(news);
                db.SaveChanges();
            }
            return null;
        }

        public ActionResult YeniHaber()
        {
            //var news = new News();
            /*
             * Var 
             * Object
             * Dynamic
             * */

            var model = new NewsViewModel()
            {
                isDeleted = true
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult YeniHaber(News news, HttpPostedFileBase file)
        {            
            string fileName = Path.GetFileName(file.FileName);
            file.SaveAs(Server.MapPath("~/Content/Uploads/" + fileName));
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var yeniHaber = new News
                    {
                        Title = news.Title,
                        ShortDescription = news.ShortDescription,
                        Content = news.Content,
                        UpdateDate = DateTime.Today,
                        IsDeleted = news.IsDeleted,
                        ViewCount = news.ViewCount,
                        Picture =  new Picture
                        {
                            
                            Title = news.Title,
                            Description = news.Title,
                            Path = "/Content/Admin/",
                            FileName = fileName                                 
                        }
                    };
                   
                    db.News.Add(yeniHaber);
                    db.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Response.Write(string.Format("Entity türü \"{0}\" şu hatalara sahip \"{1}\" Geçerlilik hataları:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Response.Write(string.Format("- Özellik: \"{0}\", Hata: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                    Response.End();
                }
            }

            return RedirectToAction("YeniHaber");
        }
    }
}