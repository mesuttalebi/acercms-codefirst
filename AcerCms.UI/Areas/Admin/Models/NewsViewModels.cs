﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AcerCms.UI.Areas.Admin.Models
{
    public class NewsViewModel
    {
        public int Id { get; set; }
        
        [Display(Name="Başlık", Prompt = "Başlığı Buraya Giriniz!")]
        [StringLength(200, ErrorMessage="{0} en fazla {1} karakter uzunluğunda olabilir!")]
        [Required(ErrorMessage= "{0} gerekli!")]
        public string Title { get; set; }
        public string ShortDescription { get; set; }

        [AllowHtml]
        public string Content { get; set; }

        [DisplayFormat(DataFormatString = "{0: d}", ApplyFormatInEditMode = true)]
        public DateTime UpdateDate { get; set; }        
        public int ViewCount { get; set; }

        public bool isDeleted { get; set; }
        //Relation
        public int? PictureId { get; set; }
        public virtual PictureViewModel Picture { get; set; }
    }
}