﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AcerCms.Dal;
using AcerCms.Dal.Enities;

namespace AcerCms.UI.Areas.Admin.ModelServices
{
    public class HaberModelService
    {
        private static int pageSize = 10;
        internal static List<News> GetNews(int page)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.News.OrderBy(x=> x.Id).Skip((page-1) * pageSize).Take(pageSize).ToList();
            }
        }
    }
}