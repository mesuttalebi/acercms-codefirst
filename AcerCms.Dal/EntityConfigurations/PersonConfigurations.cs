﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcerCms.Dal.Enities;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AcerCms.Dal.EntityConfigurations
{
    public class PersonConfigurations : EntityTypeConfiguration<Person>
    {
        public PersonConfigurations()
        {
            this.ToTable("AspNetUsers");

            this.Property(p => p.Sicil).HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(
                    new IndexAttribute("IX_Sicil", 1)
                    {
                        IsUnique = true
                    }));


            this.Property(p => p.FullName).HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(
                    new IndexAttribute("IX_Sicil", 2)
                    {
                        IsUnique = true
                    }));

            this.HasRequired(p => p.ApplicationUser).WithRequiredPrincipal(u => u.Person).WillCascadeOnDelete(true);                                 
        }
    }
}
