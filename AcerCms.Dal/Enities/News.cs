﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace AcerCms.Dal.Enities
{
    public class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }        
        public string Content { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public int ViewCount { get; set; }
        

        //Relation
        public int? PictureId { get; set; }        
        public virtual Picture Picture { get; set; }

        public string PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}


