namespace AcerCms.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Person1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.News", "PersonId", c => c.String(maxLength: 128));
            CreateIndex("dbo.News", "PersonId");
            AddForeignKey("dbo.News", "PersonId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.News", "PersonId", "dbo.AspNetUsers");
            DropIndex("dbo.News", new[] { "PersonId" });
            DropColumn("dbo.News", "PersonId");
        }
    }
}
