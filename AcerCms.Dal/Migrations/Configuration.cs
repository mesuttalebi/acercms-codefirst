using System.Diagnostics;
using AcerCms.Dal.Enities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AcerCms.Dal.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AcerCms.Dal.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AcerCms.Dal.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //if (!Debugger.IsAttached)
            //    Debugger.Launch();

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //Seed Roles
            context.Roles.AddOrUpdate(x => x.Name, new[]
            {
                new IdentityRole() {Id = "1", Name = "Administrator"},
            });


            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
           
            if (!manager.Users.Any(x => x.Email == "admin@acercms.com"))
            {
                var user = new ApplicationUser()
                {
                    Email = "admin@acercms.com",
                    UserName = "admin@acercms.com"
                };

                var res = manager.Create(user, "Admin123");


                if (res.Succeeded)
                {
                    var res2 = manager.AddToRole(user.Id, "Administrator");

                }
            }

        }
    }
}
