﻿using System;
using System.Data.Entity;
using AcerCms.Dal.Enities;
using AcerCms.Dal.EntityConfigurations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AcerCms.Dal
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<News> News { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Inbox> InboxMessages { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Person> Persons { get; set; }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
            modelBuilder.Conventions.Add(new CustomConvention());

            modelBuilder.Configurations.Add(new NewsConfigurations());
            modelBuilder.Configurations.Add(new PictureConfigurations());
            modelBuilder.Configurations.Add(new PersonConfigurations());            
        }
    }
}
